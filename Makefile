compile:
	gcc H3.c -o H3.o -lm -O3 -Wall -pedantic -fPIC
debug_rho:
	gcc H3.c -o H3.o -lm -O3 -Wall -pedantic -fPIC -D DEBUG_RHO

debug_g:
	gcc H3.c -o H3.o -lm -O3 -Wall -pedantic -fPIC -D DEBUG_G

debug_numerov:
	gcc H3.c -o H3.o -lm -O3 -Wall -pedantic -fPIC -D DEBUG_NUMEROV

debug_epsilon:
	gcc H3.c -o H3.o -lm -O3 -Wall -pedantic -fPIC -D DEBUG_EPSILON

debug_all:
	gcc H3.c -o H3.o -lm -O3 -Wall -pedantic -fPIC -D DEBUG_RHO -D DEBUG_G -D DEBUG_NUMEROV -D DEBUG_EPSILON
