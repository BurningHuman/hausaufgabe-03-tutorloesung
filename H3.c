#include <stdio.h>
#include <stdlib.h>

#define EPSILON_STEP 0.1
#define RHO_PRECISION 1e-12

//Global Constants for the code
double lower_boundary = 0;
double upper_boundary = 20;
double W = 1;
double kappa = 2;
double epsilon_max = 9.;
double h = 0.01;


//Brent-Decker root searching algorithm, which combines the bisection and secant algorithm in one.
int BrentDecker_SearchRoot
	(
		double (*f) ( double *, double *, double, double, double, unsigned int ), 
		int	steps, 
		double	*R, 
		double	*g, 
		double	xEps, 
		double	lower_boundary, 
		double	upper_boundary
	)
{
	double a, b, c;
	double fa, fb, fc;
	double m, p, q;
	double lb = lower_boundary;
	double ub = upper_boundary;

	//a = lb; fa = f( steps, R_0, R_1, R, g, a );
	a = lb; fa = f( R, g, a, epsilon, steps);
	//b = ub; fb = f( steps, R_0, R_0, R, g, b ); lb = b;
	b = ub; fb = f( R, g, b, epsilon, steps ); lb = b;
	c = a; fc = fa;
	if (abs(fc)<abs(fb))
	{
		a = b; fa = fb;
		b = c; fb = fc; lb = b;
		c = a; fc = fa;
	}
	m = 0.5 * ( c + b );
	while (abs(m-b)>xEps)
	{
		printf("a=%lf\tb=%lf\tfa=%lf\tfb=%lf\n", a, b, fa, fb);
		p = ( b - a ) * fb;
		if (p>=0)
		{
			q = fa - fb;
		}
		else 
		{
			q = fb - fa;
			p = -p;
		}
		a = b; fa = fb;
		if (p<=abs(q)*xEps) 
		{ 
			if(c>b) 
			{
				b += xEps;
			} 
			else
			{
				b -= xEps;
			}
		}
		else 
		{
			if (p<(m-b)*q) 
			{
				b = p / q + b;
			} 
			else 
			{
				b = m;
			}
		}
		lb = b; fb = f( steps, R_0, R_1, R, g, b );
		if (fc*fb>=0) 
		{
			c = a; fc = fa;
		}
		if (abs(fc)<abs(fb))
		{
			a = b; fa = fb;
			b = c; fb = fc; lb = b;
			c = a; fc = fa;
		}
		m = 0.5 * ( c + b );
	}
	ub = c;
	if (fc>=0) 
	{
		return fb<=0;
	} 
	else 
	{
		return fb>=0;
	}
}

void init_rho ( long n, double h, double lower_boundary, double *rho ) {
	/* Initialisiere Stuetzpunkte */ 
	for (unsigned long i=0; i<=n; i++) 
	{
		rho[i] = lower_boundary + i * h;
	#ifdef DEBUG_RHO
		printf("%lf\n", rho[i]);
	#endif
	}

}

void init_g ( unsigned long n, unsigned char l, double hh12, double *rho, double *g ) 
{
	//u(r) regulaer am Ursprung	
	g[0] = 0; 
	for (unsigned long i=1; i<=n; i++) 
	{
		g[i] = ( - l * ( l + 1 ) / rho[i] / rho[i] - W * rho[i]*rho[i] ) * hh12;
	#ifdef DEBUG_G
		printf("%lf\n", g[i]);
	#endif
	}
}

//Numerov Algorithm
double numerov(double *y, double *g, double local_lower_boundary, double epsilon, unsigned int steps)
{
	y[0] = local_lower_boundary;
	y[1] = local_lower_boundary+h;

	g[0] += epsilon;
	g[1] += epsilon;
	double hh12 = h*h/12.;
	for(unsigned int i = 2; i < steps; i++)
	{
		g[i] += epsilon; 	
		double pre_factor = 1./(1.+hh12*g[i]);
		
		//Those are all prefactors for y[i-1] and y[i-2]
		double y_im1 =  2.*(1.-5.*hh12*g[i-1]);
		double y_i = 1.+hh12*g[i-2];

		//The tail with all the s functions
		//Which is actually not existent in this case
		//double tail = hh12*(s[i] + 10 * s[i-1] + s_[i-2]);
		
		//y[i+1] = pre_factor * (y_im1 * y[i-1] - y_i * y_[i-2] + tail); 
		y[i+1] = pre_factor * (y_im1 * y[i-1] - y_i * y[i-2]); 
		#ifdef DEBUG_NUMEROV
			printf("pre_factor:%lf\ty_im1:%lf\ty_i:%lf\ty[i+1]:%lf\n",pre_factor, y_im1, y_i, y[i+1]);
		#endif
	}
	//Returning the right boundary of the calculated function, useful for the Brent-Decker-Algorithm
	return y[steps-1];
}

int main(void)
{
	//Factor of the numerov equation
	double hh12 = h*h/12;
	unsigned int steps = (int) upper_boundary/h;
	double epsilon;

	double *rho = (double *)malloc(steps*sizeof(double));
	double *g = (double *)malloc(steps*sizeof(double));
	double *R = (double*)malloc(steps*sizeof(double));
	
	init_rho(steps, h, lower_boundary, rho);

	for(unsigned char l = 0; l < 1; l++)
	{
		init_g(steps, l, hh12, rho, g);
		for(double epsilon_cur = 0.0; epsilon_cur <= epsilon_max; epsilon_cur+EPSILON_STEP)
		{
			epsilon = BrentDecker_SearchRoot(numerov, steps, R, g, RHO_PRECISION, lower_boundary, upper_boundary);
			//numerov(rho, g, lower_boundary, epsilon_cur, h, steps)
			#ifdef DEBUG_EPSILON
				printf("epsilon: %lf\n", epsilon);
			#endif
		}
	}
	//printf();
	return 0;
}
